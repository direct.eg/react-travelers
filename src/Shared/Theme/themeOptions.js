import {createTheme} from "@mui/material";

export function getTheme(mode) {
    return createTheme({
        palette: {
            mode: mode,
            primary: {
                main: '#151c43',
                light: '#999ba2',
            },
            secondary: {
                main: '#2e8130',
                light: '#b3ceb3',
                dark: '#173117',
            },
            background: {
                default: '#91a0ff',
            },
        },
        typography: {
            h1: {
                fontSize: 30,
            },
        },
    })
}