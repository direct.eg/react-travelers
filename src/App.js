import React, {createContext, useState} from 'react'
import './App.css';
import {TopNav} from "./Components/Layout/TopNav";
import {Container, CssBaseline, ThemeProvider} from "@mui/material";
import {getTheme} from "./Shared/Theme/themeOptions";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {TripList} from "./Components/Trip/TripList";
import {Counter} from "./Components/Examples/Counter";
import {BOTripList} from "./Components/Trip/BOTripList";
import {Provider} from "react-redux";
import {Calculator2} from "./Components/Examples/Calculator2";
import {Power} from "./Components/Examples/Power";
import {store} from "./Stores/store";
import {BOTripShow} from "./Components/Trip/BOTripShow";
import {BOTripForm} from "./Components/Trip/BOTripForm";

export const ChangeThemeContext = createContext(null);

function App() {
    const [themeMode, setThemeMode] = useState('light');

    return (
        <React.StrictMode>
            <ThemeProvider theme={getTheme(themeMode)}>
                <CssBaseline/>
                <BrowserRouter>
                    <ChangeThemeContext.Provider value={() => {
                        setThemeMode(themeMode === 'light' ? 'dark' : 'light')
                    }}>
                        <Provider store={store}>
                            <TopNav/>
                            <Container>
                                <main>
                                    <Routes>
                                        <Route path={'/'} element={<TripList/>}/>
                                        <Route path={'/compteur'} element={<Counter/>}/>
                                        <Route path={'/admin/trip'} element={<BOTripList/>}/>
                                        <Route path={'/admin/trip/new'} element={<BOTripForm/>}/>
                                        <Route path={'/admin/trip/:id'} element={<BOTripShow/>}/>
                                        <Route path={'/admin/trip/:id/edit'} element={<BOTripForm/>}/>
                                        <Route path={'/calculator'} element={<Calculator2/>}/>
                                        <Route path={'/power'} element={<Power/>}/>
                                    </Routes>
                                </main>
                            </Container>
                        </Provider>
                    </ChangeThemeContext.Provider>
                </BrowserRouter>
            </ThemeProvider>
        </React.StrictMode>
    )
}

export default App;
