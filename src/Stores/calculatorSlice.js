import {createSlice} from "@reduxjs/toolkit";

export const calculatorSlice = createSlice({
    name: 'calculator',
    initialState: {result: 1},
    reducers: {
        calculate: state => {
            console.log('DISPATCH CALCULATE');
            state.result = state.result * 5;
        }
    }
})

export const {calculate} = calculatorSlice.actions;
export default calculatorSlice.reducer;