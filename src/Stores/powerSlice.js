import {createSlice} from "@reduxjs/toolkit";

export const powerSlice = createSlice({
    name: 'power',
    initialState: {value: 2},
    reducers: {
        upPower: state => {
            state.value = state.value * state.value;
            // state.value = state.value ** 2; <= NE PAS RECOPIER
            // state.value = Math.pow(state.value, 2); <= NE PAS RECOPIER
        }
    }
})

export const {upPower} = powerSlice.actions;
export default powerSlice.reducer;