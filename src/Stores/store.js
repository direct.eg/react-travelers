import {configureStore} from "@reduxjs/toolkit";
import calculatorReducer from './calculatorSlice';
import {counterReducer} from "./counterSlice";
// le terme "powerSliceReducer" est choisi arbitrairement
import powerSliceReducer from './powerSlice'

export const store = configureStore({
    reducer: {
        counter: counterReducer,
        calculator: calculatorReducer,
        power: powerSliceReducer,
    }
})