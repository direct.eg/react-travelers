import {createSlice} from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {value: 0},
    reducers: {
        incrementCounter: initialState => {
            initialState.value = initialState.value + 1;
        },
        upCounter: function (state, action) {
            // On récupère la valeur envoyée
            const payload = Number(action.payload);
            // On met la valeur du store
            state.value = state.value + payload;
        }
    }
})

export const {incrementCounter, upCounter} = counterSlice.actions;
export const counterReducer = counterSlice.reducer;
