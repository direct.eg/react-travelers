import {AppBar, Box, Button, Dialog, DialogContent, DialogTitle, Stack, Toolbar, Typography} from "@mui/material";
import {Link, useNavigate} from "react-router-dom";
import {SwitchTheme} from "./SwitchTheme";
import {useEffect, useState} from "react";
import {LoginForm} from "../Auth/LoginForm";
import {useSelector} from "react-redux";
import jwtDecode from "jwt-decode";

export function TopNav(props) {
    const [dialogLoginOpen, setDialogLoginOpen] = useState(false);
    const [user, setUser] = useState(null);

    const connectUser = () => {
        const token = sessionStorage.getItem('token');

        if (token) {
            const decodedUser = jwtDecode(token);
            setUser(decodedUser);
        }
    }

    useEffect(() => {
        connectUser();
    }, [])

    const navigate = useNavigate();
    const token = sessionStorage.getItem('token');

    const counter = useSelector(state => state.counter.value);
    const result = useSelector(state => state.calculator.result);
    const power = useSelector(state => state.power.value);


    return (
        <>
            <Box sx={{flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <Stack direction={'row'} justifyContent={'space-between'} sx={{width: '100%'}}
                               alignItems={'center'}>
                            <Stack direction={'row'} gap={2}>
                                <Link to={'/'} style={{color: 'white', textDecoration: 'none'}}>Voyages</Link>
                                {/*<Link to={'/calculator'} style={{color: 'white', textDecoration: 'none'}}>Calculatrice</Link>*/}
                                <Link to={'/compteur'} style={{color: 'white', textDecoration: 'none'}}>Compteur</Link>
                                {/*<Link to={'/power'} style={{color: 'white', textDecoration: 'none'}}>Puissance de 2</Link>*/}
                                {
                                    token
                                        ? <Link to={'/admin/trip'} style={{color: 'white', textDecoration: 'none'}}>
                                            Back office
                                        </Link>
                                        : ''
                                }
                            </Stack>
                            <Stack direction={'row'} gap={2}>
                                <SwitchTheme/>
                                <Typography sx={{color: 'white'}}>{counter}</Typography>
                                {/*<Typography sx={{color: 'white'}}>Résultat : {result}</Typography>*/}
                                {/*<Typography sx={{color: 'white'}}>Puissance de 2 : {power}</Typography>*/}

                                {
                                    !user && (
                                        <Button
                                            color="inherit"
                                            onClick={() => {
                                                setDialogLoginOpen(true)
                                            }}
                                        >Connexion</Button>
                                    )
                                }
                            </Stack>

                            {
                                user && (
                                    <>
                                        <Typography sx={{color: 'white'}}>{user.email}</Typography>
                                        <Button onClick={() => {
                                            sessionStorage.removeItem('token');
                                            setUser(null);
                                            navigate('/');
                                        }} color={'secondary'}>Déconnexion</Button>
                                    </>

                                )
                            }
                        </Stack>
                    </Toolbar>
                </AppBar>
            </Box>
            <Dialog open={dialogLoginOpen} onClose={() => {
                setDialogLoginOpen(false)
            }}>
                <DialogTitle>
                    Connexion
                </DialogTitle>
                <DialogContent>
                    <LoginForm handleLoginSuccess={() => {
                        setDialogLoginOpen(false);
                        connectUser();
                        navigate('/admin/trip');
                    }}/>
                </DialogContent>
            </Dialog>
        </>
    );
}