import {Box, Switch} from "@mui/material";
import {useContext} from "react";
import {ChangeThemeContext} from "../../App";

export function SwitchTheme() {
    const onThemeModeChanged = useContext(ChangeThemeContext);

    return (
        <Box>
            <Switch
                color={'secondary'}
                onClick={onThemeModeChanged}
            />
        </Box>
    )
}