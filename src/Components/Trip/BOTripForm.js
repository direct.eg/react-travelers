import {Button, Container, Paper, Stack, TextField, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {TripApiService} from "../../Services/Trip/TripApiService";
import {useNavigate, useParams} from "react-router-dom";

export function BOTripForm() {
    const urlParams = useParams();
    const newTrip = {
        country: '',
        duration: '',
        city: '',
        beginAt: '',
        endAt: '',
        isFinished: '',
    }

    const [trip, setTrip] = useState(newTrip);
    const navigate = useNavigate();

    useEffect(() => {
        if (urlParams.id) {
            // On est en ajout
            TripApiService.find(urlParams.id).then(res => {
                setTrip(res.data)
            })
        }
    }, []);

    return (
        <Container>
            <Typography variant={'h1'} sx={{mt: 2}}>{urlParams.id ? "Edition" : "Ajout"} d'un voyage</Typography>

            <Paper sx={{my: 2, p: 2}}>
                <form
                    onSubmit={event => {
                        // On empêche la soumission du formulaire
                        event.preventDefault();

                        if (urlParams.id) {
                            // Modification
                            TripApiService
                                .update(trip.id, trip)
                                .then(res => {
                                    navigate('/admin/trip/' + res.data.id)
                                })
                            ;
                        } else {
                            // Ajout
                            TripApiService
                                .addTrip(trip)
                                .then(res => {
                                    navigate('/admin/trip/' + res.data.id)
                                })
                            ;
                        }
                    }}
                >
                    <Stack gap={2}>
                        <TextField label={'Ville'} value={trip.city} onChange={event => {
                            const value = event.target.value;
                            setTrip({
                                ...trip,
                                city: value
                            })
                        }}/>
                        <TextField label={'Pays'} value={trip.country} onChange={event => {
                            const value = event.target.value;
                            setTrip({
                                ...trip,
                                country: value
                            })
                        }}/>
                        <TextField label={'Durée du voyage'} value={trip.duration} onChange={event => {
                            const value = event.target.value;
                            setTrip({
                                ...trip,
                                duration: value
                            })
                        }}/>

                        <Button variant={'outlined'} color={'primary'} type={'submit'}>{urlParams.id ? "Modifier" : "Ajouter"} le voyage</Button>
                    </Stack>
                </form>
            </Paper>
        </Container>
    )
}