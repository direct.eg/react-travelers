import {IconButton, Stack, TableCell, TableRow} from "@mui/material";
import {Link} from "react-router-dom";
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteIcon from '@mui/icons-material/Delete';
import {TripApiService} from "../../Services/Trip/TripApiService";
import {Edit} from "@mui/icons-material";

function TripLine(props) {
    const trip = props.trip;
    // const {trip} = props;

    const removeTrip = () => {
        TripApiService
            .delete(trip.id)
            .then(res => {
                props.afterRemove();
            })
        ;
    }

    return (
        <TableRow>
            <TableCell>{trip.country}</TableCell>
            <TableCell>{trip.duration}</TableCell>
            <TableCell>
                <Stack flexDirection={'row'} alignItems={'center'}>
                    <Link to={'/admin/trip/' + trip.id}>
                        <VisibilityIcon color={'primary'}/>
                    </Link>
                    <Link to={'/admin/trip/' + trip.id + '/edit'}>
                        <Edit color={'primary'}/>
                    </Link>
                    <IconButton onClick={removeTrip}>
                        <DeleteIcon color={'error'}/>
                    </IconButton>
                </Stack>

            </TableCell>
        </TableRow>
    )
}

export default TripLine;