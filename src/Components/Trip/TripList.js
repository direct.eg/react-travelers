import Container from "../Layout/Container";
import {useLastTrips} from "../../Hooks/Trip/useLastTrips";
import {Card, CardContent, Stack, Typography} from "@mui/material";

export function TripList() {

    const trips = useLastTrips();

    if (!trips) {
        return <Container>
            Chargement en cours
        </Container>
    }

    return (
        <Container>
            {
                trips.length > 0 && (
                    <Stack flexDirection={'row'} gap={2} mt={2}> {/* div avec "display: flex" */}
                        {
                            trips.map((trip, index) => (
                                <Card key={'trip-' + index} sx={{flexGrow: 1}}>
                                    <CardContent>
                                        <Typography variant={'h3'}>
                                            {trip.country}
                                        </Typography>
                                    </CardContent>
                                    <CardContent>
                                        <Typography >
                                            {trip.duration}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            ))
                        }
                    </Stack>
                )
            }
        </Container>
    );
}
