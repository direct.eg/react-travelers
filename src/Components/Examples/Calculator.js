import {Button, Container} from "@mui/material";
import {useMemo, useState} from "react";

export function Calculator() {
    const [todos, setTodos] = useState([]);
    const [limit, setLimit] = useState(100000000);

    function calculate(limit) {
        let result = 0;
        for (let i = 0 ; i < limit ; i++) {
            result+= i;
        }
        return result;
    }

    const result = useMemo(() => calculate(limit), [limit]);

    return (
        <Container>
            <ul>
                {todos.map(todo => <li>{todo.label}</li>)}
            </ul>
            <Button
                onClick={() => {
                    setTodos([...todos, {label: 'Nouvelle tâche'}])
                }}
            >Ajouter une tâche</Button>

            {result}
            <Button
                onClick={() => {
                    setLimit(limit + 1)
                }}
            >
                Augmenter la limite
            </Button>
        </Container>
    )
}