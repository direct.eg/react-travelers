import {Button} from "@mui/material";
import {useDispatch} from "react-redux";
import {upPower} from "../../Stores/powerSlice";

export function Power() {

    const dispatch = useDispatch();

    return (
        <Button
            variant={'contained'}
            color={'secondary'}
            onClick={() => {
                // On augmente la puissance
                dispatch(upPower());
            }}
        >Augmenter la puissance</Button>
    )
}