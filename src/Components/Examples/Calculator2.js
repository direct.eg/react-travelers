import {Button, Container} from "@mui/material";
import {useDispatch} from "react-redux";
import {calculate} from "../../Stores/calculatorSlice";

export function Calculator2() {
    const dispatch = useDispatch();

    const calculateResult = () => {
        dispatch(calculate());
    }

    return (
        <Container>
            <Button onClick={calculateResult}>Calculer</Button>
        </Container>
    )
}