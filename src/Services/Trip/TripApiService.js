const FRONT_PATH = "https://api.sieli-lab.fr/";
const BACK_PATH = FRONT_PATH + 'admin/';

export class TripApiService {
    static findLastTrips() {
        return fetch(FRONT_PATH + 'last-trips').then(res => res.json());
    }

    /**
     * Cherche tous les voyages présents dans l'API'
     */
    static findAllTrips() {
        // Récupération du token
        const token = sessionStorage.getItem('token');
        // S'il n'y a pas token, on ne fait pas de call API
        if (!token) {
            return;
        }

        // On appelle l'API
        return fetch(
            BACK_PATH + 'trip/', // l'URL
            {
                headers: {
                    Authorization: `Bearer ${token}` // On envoie le token
                }
            }
        )
            .then(res => res.json()) // on convertit la réponse en "JSON" (objet JavaScript) pour l'exploiter en JavaScript
        ;
    }

    /**
     * Récupération du JWT (token utilisateur)
     */
    static login(email, password) {
        return fetch(
            FRONT_PATH + 'login',
            {
                method: 'POST',
                body: JSON.stringify({email: email, password: password})
            }
        ).then(res => res.json());
    }

    /**
     * Ajout d'un voyage dans l'API
     */
    static addTrip(data) {

        const token = sessionStorage.getItem('token');

        if (!token) {
            return;
        }

        let headers = new Headers();
        headers.set('Authorization', `Bearer ${token}`);

        return fetch(
            BACK_PATH + 'trip/',
            {
                method: 'POST',
                body: JSON.stringify(data),
                headers: headers
            }
        ).then(res => res.json());
    }

    static find(id) {
        // Récupération du token
        const token = sessionStorage.getItem('token');
        // S'il n'y a pas token, on ne fait pas de call API
        if (!token) {
            return;
        }

        // On appelle l'API
        return fetch(
            BACK_PATH + 'trip/' + id, // l'URL
            {
                headers: {
                    Authorization: `Bearer ${token}` // On envoie le token
                }
            }
        )
            .then(res => res.json()) // on convertit la réponse en "JSON" (objet JavaScript) pour l'exploiter en JavaScript
        ;
    }

    static delete(id) {
        // Récupération du token
        const token = sessionStorage.getItem('token');
        // S'il n'y a pas token, on ne fait pas de call API
        if (!token) {
            return;
        }

        // On appelle l'API
        return fetch(
            BACK_PATH + 'trip/' + id, // l'URL
            {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${token}` // On envoie le token
                }
            }
        )
            .then(res => res.json()) // on convertit la réponse en "JSON" (objet JavaScript) pour l'exploiter en JavaScript
            ;
    }

    static update(id, data) {
        const token = sessionStorage.getItem('token');

        if (!token) {
            return;
        }

        let headers = new Headers();
        headers.set('Authorization', `Bearer ${token}`);

        return fetch(
            BACK_PATH + 'trip/' + id,
            {
                method: 'PUT',
                body: JSON.stringify(data),
                headers: headers
            }
        ).then(res => res.json());
    }
}
